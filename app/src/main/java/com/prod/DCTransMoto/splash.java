package com.prod.DCTransMoto;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class splash extends AppCompatActivity {


    Animation abajo, arriba;
    ImageView globito, saludo;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        saludo =  (ImageView) findViewById(R.id.imageView2);
        abajo = AnimationUtils. loadAnimation(this,R.anim.desdeabajo);
        saludo.setAnimation(abajo);

        globito =  (ImageView) findViewById(R.id.imageView);
        arriba = AnimationUtils. loadAnimation(this,R.anim.desdearriba);
        globito.setAnimation(arriba);


        new Handler().postDelayed(new Runnable() {
            @Override


            public void run() {
                Intent ir = new Intent(splash.this, MainActivity.class);
                startActivity(ir);
                finish();

            }

        } ,4000);

    }
}
