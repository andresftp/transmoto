package com.prod.DCTransMoto;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prod.DCTransMoto.adapters.AdaptadorConductor;
import com.prod.DCTransMoto.models.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class hoteles extends AppCompatActivity{

    //Inicializacion de  editex y recycler view
    EditText etBuscador;
    RecyclerView rvLista;

    //Configuramos el adaptador de productos
    AdaptadorConductor adaptador;
    List<Usuario> listaUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoteles2);

        etBuscador = findViewById(R.id.etBuscador);

        etBuscador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtrar(s.toString());

            }
        });

        //Seteamos el recycler view
        rvLista = findViewById(R.id.rvLista);

        rvLista.setLayoutManager(new GridLayoutManager(this, 1));

        listaUsuarios = new ArrayList<>();


        //Seteamos el adaptador de productos
        obtenerProductos();

        adaptador = new AdaptadorConductor(hoteles.this, listaUsuarios);


        rvLista.setAdapter(adaptador);


    }

    public void obtenerProductos(){
        //Creamos el request en metodo post para obtenener la consulta
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, getResources().getString(R.string.URL_PRODUCTO),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //Procesamos el json arrany que se obtuvo en la consulta
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            //Recorremos el json array que contene la información consultada
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                listaUsuarios.add(
                                        //Seteamos el modelo producto con la información obtenida en la consulta
                                        new Usuario(
                                                jsonObject1.getString("id_usuario"),
                                                jsonObject1.getString("nombres"),
                                                jsonObject1.getString("apellidos"),
                                                jsonObject1.getString("email"),
                                                jsonObject1.getString("telefono"),
                                                jsonObject1.getString("marca"),
                                                jsonObject1.getString("modelo"),
                                                jsonObject1.getString("placa"),
                                                jsonObject1.getString("color"),
                                                jsonObject1.getString("imagen_url")
                                        )
                                );
                            }

                            //Seteamos en el adaptador de productos el resultado obtenido de la consulta
                            adaptador = new AdaptadorConductor(hoteles.this, listaUsuarios);
                            rvLista.setAdapter(adaptador);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        );

        requestQueue.add(stringRequest);

    }


    //creamo la funcion de filtrado
    public void filtrar(String texto){
        ArrayList<Usuario> filtrarLista = new ArrayList<>();

        //Hacemos un recorrido por el array de productos y comparamos el texto buscado con la lista
        for (Usuario producto: listaUsuarios){
            if(producto.getNombres().toLowerCase().contains(texto.toLowerCase())){
                filtrarLista.add(producto);
            }
        }


        //Seteamos el adaptador con el contenido filtrado
        adaptador.filtrar(filtrarLista);
    }

    public void Notifications(View view) {
    }




}






