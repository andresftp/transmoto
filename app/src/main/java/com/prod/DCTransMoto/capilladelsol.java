package com.prod.DCTransMoto;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class capilladelsol extends AppCompatActivity {
    ViewPager viewPager;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capilladelsol);
        viewPager = (ViewPager) findViewById(R.id.MyViewPager);
        ViewPageAdapter3 viewPageAdapter3 = new ViewPageAdapter3(this);
        viewPager.setAdapter(viewPageAdapter3);
    }


    public void informacioncapilla(View view) {
        Intent hot = new Intent(capilladelsol.this, informacioncapillasol.class);
        startActivity(hot);
    }

        public void whatsapp4(View view) {
            Uri uri = Uri.parse("smsto:" + "3208802310");
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setPackage("com.whatsapp");
            startActivity(i);

    }
}
