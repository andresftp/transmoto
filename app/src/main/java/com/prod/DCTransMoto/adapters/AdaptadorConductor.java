package com.prod.DCTransMoto.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import com.prod.DCTransMoto.DetalleActivity;
import com.prod.DCTransMoto.R;
import com.prod.DCTransMoto.classes.ItemclickListener;
import com.prod.DCTransMoto.models.Usuario;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorConductor extends RecyclerView.Adapter<AdaptadorConductor.ConductorViewHolder> {

    Context context;
    RequestQueue request;
    List<Usuario> listaUsuarios;
    View mView;

    public AdaptadorConductor(Context context, List<Usuario> listaUsuarios) {
        this.context = context;
        this.listaUsuarios = listaUsuarios;
        request = Volley.newRequestQueue(context);
    }

    @NonNull
    @Override
    public ConductorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rv_conductor, viewGroup, false);

        return  new ConductorViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ConductorViewHolder conductorViewHolder, int i) {
        conductorViewHolder.tvIdUsuario.setText(listaUsuarios.get(i).getId_usuario());
        conductorViewHolder.tvNombre.setText(listaUsuarios.get(i).getNombres());
        conductorViewHolder.tvApellido.setText(listaUsuarios.get(i).getApellidos());


        //Verificamos si tiene una imagen o no

        if(listaUsuarios.get(i).getFoto()=="null"){
            conductorViewHolder.tvImagen.setImageResource(R.drawable.profile_cond);

        }else{
            cargarImagenWebService(listaUsuarios.get(i).getFoto(), conductorViewHolder);

        }


        conductorViewHolder.setItemclickListener(new ItemclickListener() {
            @Override
            public void onItemClickListener(View v, int position) {
                String gNom = listaUsuarios.get(position).getNombres();
                String gApe = listaUsuarios.get(position).getApellidos();
                String gTel = listaUsuarios.get(position).getTelefono();
                String gMarc = listaUsuarios.get(position).getMarca();
                String gMod = listaUsuarios.get(position).getModelo();
                String gPlc = listaUsuarios.get(position).getPlaca();
                String gCol = listaUsuarios.get(position).getColor();
                String gFoto = listaUsuarios.get(position).getFoto();


                //ByteArrayOutputStream stream =  new ByteArrayOutputStream()

                Intent intent =  new Intent(context, DetalleActivity.class);
                intent.putExtra("iNom",gNom);
                intent.putExtra("iApe",gApe);
                intent.putExtra("iTel",gTel);
                intent.putExtra("iMarc",gMarc);
                intent.putExtra("iMod",gMod);
                intent.putExtra("iPlc",gPlc);
                intent.putExtra("iCol",gCol);
                intent.putExtra("iFoto",gFoto);

                context.startActivity(intent);

            }
        });

    }

    private void cargarImagenWebService(String rutaImagen, final ConductorViewHolder conductorViewHolder) {

        String urlImagen = rutaImagen;
        urlImagen= urlImagen.replace(" ","%20");

        ImageRequest imageRequest = new ImageRequest(urlImagen, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                conductorViewHolder.tvImagen.setImageBitmap(response);

            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error al cargar la imagen",Toast.LENGTH_SHORT).show();
            }
        });

        request.add(imageRequest);
    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }




    public class ConductorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemclickListener itemclickListener;

        TextView tvIdUsuario, tvNombre, tvApellido;
        ImageView tvImagen;
        public ConductorViewHolder(@NonNull View itemView) {
            super(itemView);
            mView =itemView;
            tvIdUsuario = itemView.findViewById(R.id.tvIdUsuario);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvApellido = itemView.findViewById(R.id.tvApellido);
            tvImagen = (ImageView) itemView.findViewById(R.id.tvImagen);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemclickListener.onItemClickListener(v, getLayoutPosition());

        }

        public void  setItemclickListener(ItemclickListener ic){
            this.itemclickListener = ic;
        }
    }

    public void filtrar(ArrayList<Usuario> filtroConductor){
        this.listaUsuarios = filtroConductor;
        notifyDataSetChanged();
    }
}
