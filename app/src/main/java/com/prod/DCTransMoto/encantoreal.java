package com.prod.DCTransMoto;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class encantoreal extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encantoreal);

        viewPager = (ViewPager) findViewById(R.id.MyViewPager);

        ViewPageAdapter5 viewPageAdapter5 = new ViewPageAdapter5(this);
        viewPager.setAdapter(viewPageAdapter5);

    }

    public void informacionyesica (View view) {
        Intent hot = new Intent(encantoreal.this, informacionyesica.class);
        startActivity(hot);
    }

    public void whatsapp6(View view) {
        Uri uri = Uri.parse("smsto:" + "3213397666");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(i);

    }
}
