package com.prod.DCTransMoto;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class botalon extends AppCompatActivity {
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botalon);

        viewPager = (ViewPager) findViewById(R.id.MyViewPager);

        ViewPageAdapter9 viewPageAdapter9 = new ViewPageAdapter9(this);
        viewPager.setAdapter(viewPageAdapter9);
    }


    public void informacionbota(View view) {
        Intent hot = new Intent(botalon.this, informacionbotalon.class);
        startActivity(hot);

    }

}
