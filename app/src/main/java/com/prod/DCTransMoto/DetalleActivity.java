package com.prod.DCTransMoto;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

public class DetalleActivity extends AppCompatActivity {
    TextView tit_nombre, tit_tel, tit_marca, tit_modelo, tit_color;
    String numtel;
    ImageView img_detail;
    Button btnCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        tit_nombre = findViewById(R.id.tit_nombre);
        tit_tel = findViewById(R.id.tit_tel);
        img_detail = findViewById(R.id.img_detail);
        tit_marca = findViewById(R.id.tit_marca);
        tit_modelo = findViewById(R.id.tit_modelo);
        tit_color = findViewById(R.id.tit_color);
        btnCall = (Button) findViewById(R.id.btnCall);

        Intent intent = getIntent();

        String iNom = intent.getStringExtra("iNom");
        String iApe = intent.getStringExtra("iApe");
        String iTel = intent.getStringExtra("iTel");
        String iMarc = intent.getStringExtra("iMarc");
        String iMod = intent.getStringExtra("iMod");
        String iCol = intent.getStringExtra("iCol");
        String iFoto = intent.getStringExtra("iFoto");

        numtel = iTel;
        tit_nombre.setText("Nombres: " + iNom + " " + iApe);
        tit_tel.setText("Teléfono: " +iTel);
        tit_marca.setText("Marca: " +iMarc);
        tit_modelo.setText("Modelo: " +iMod);
        tit_color.setText("Color: " +iCol);

        if(iFoto.equals("null")){
            img_detail.setImageResource(R.drawable.profile_cond);

        }else{
            cargarImagenWebService(iFoto);

        }




        btnCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                callPhoneNumber(numtel);

            }

        });

    }

    private void cargarImagenWebService(String rutaImagen) {

        String urlImagen = rutaImagen;
        urlImagen= urlImagen.replace(" ","%20");

        ImageRequest imageRequest = new ImageRequest(urlImagen, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                img_detail.setImageBitmap(response);

            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetalleActivity.this, "Error al cargar la imagen",Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue request = Volley.newRequestQueue(DetalleActivity.this);
        request.add(imageRequest);
    }

    public void callPhoneNumber(String num_tel)
    {
        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    ActivityCompat.requestPermissions(DetalleActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 101);

                    return;
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + num_tel));
                startActivity(callIntent);

            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + num_tel));
                startActivity(callIntent);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        if(requestCode == 101)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                callPhoneNumber(numtel);
            }
            else
            {
                Log.e("TAG", "Permission not Granted");
            }
        }
    }
}
