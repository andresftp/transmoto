package com.prod.DCTransMoto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {
    //Inicializacion de  editex y de boton
    EditText edtCorreo, edtNombres, edtApellidos, edtTelefono, editMarcaMoto, editModelo, editPlaca, editColor;
    Button btnRegister, btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Seteo de información de formulario en variables
        edtCorreo = findViewById(R.id.editCorreo);
        edtNombres = findViewById(R.id.editNombres);
        edtApellidos = findViewById(R.id.editApellidos);
        edtTelefono = findViewById(R.id.editTelefono);
        editMarcaMoto = findViewById(R.id.editMarcaMoto);
        editModelo = findViewById(R.id.editModelo);
        editPlaca = findViewById(R.id.editPlaca);
        editColor = findViewById(R.id.editColor);
        btnRegister = findViewById(R.id.btnRegister);
        btnCancelar = findViewById(R.id.btnCancelar);

        //Se crea el evento dle boton registrar
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamamos al funcion de guardado del formulario y seteamos la url
                Log.d("STATE", "onClick: Registro");
                registroFormulario(getResources().getString(R.string.URL_REGISTRA_USUARIO));
            }
        });

        //Se crea el evento clic del boton cancelar
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });



    }

    //Metodo para procesar el formulario
    private void registroFormulario(String URL){
        //Uso de string request para establecer el metodo en el cual se le va a enviar la data al servicio
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DATA REGISTRO",response);
                //Validanos que el response no esté vacio
                if(!response.isEmpty()){

                    //Realizamos un parseo a la rspuesta para verificar el inicio de sesion
                    JSONObject obj = null;
                    String resp = null;
                    String msj = null;
                    try {
                        obj = new JSONObject(response);
                        resp = obj.getString("resp");
                        msj = obj.getString("msj");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Verificamos si elservicio devuelve un error
                    if(resp.equals("error")){
                        Toast.makeText(RegistroActivity.this, msj,Toast.LENGTH_SHORT).show();
                    }else{
                        // Si todo secumple mediante un intent lanzamos la actividad del menu principal
                        Intent intent = new Intent(getApplicationContext(), hoteles.class);
                        startActivity(intent);
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Mostramos error en caso de que algo no funcione correctamente
                Toast.makeText(RegistroActivity.this,error.toString(),Toast.LENGTH_SHORT).show();

            }
        }){
            //Establecemos los parametros que el servicio necesita para darnos respuesta
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Mapeamos los parametros
                Map<String,String> parametros = new HashMap<String, String>();

                String tipo = "ix";

                //Mediante el metodo put enviamos los datos al servicio
                parametros.put("correo",edtCorreo.getText().toString());
                parametros.put("nombres",edtNombres.getText().toString());
                parametros.put("apellidos",edtApellidos.getText().toString());
                parametros.put("telefono",edtTelefono.getText().toString());
                parametros.put("marca",editMarcaMoto.getText().toString());
                parametros.put("modelo",editModelo.getText().toString());
                parametros.put("placa",editPlaca.getText().toString());
                parametros.put("color",editColor.getText().toString());
                parametros.put("type",tipo);
                //Retornamos los parametros mediante la instancia creada
                return parametros;
            }

        };

        // Con la clase request creamos na instancia con los parametros
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Agregamos la instancia del objeto string reques para procesar las peticiones desde nuestra app
        requestQueue.add(stringRequest);
    }
}
